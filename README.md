# Test suite for Bcash Redirection to Bcash-api-migration #


This repository contains the files to test the redirection from Bcash to Bcash_api_migration, the result of these tests and the request layer cURL commands.

### Contains ###

* Postman collection of petitions
* Response to each request
* requests cURL command